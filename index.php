<?php @include 'header.php' ?>

<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->
<div class="overlay">
    <div class="spinner"></div>
</div>
<div data-spy="scroll" data-target="#scrollSpy" data-offset="">
    <section id="section1" class="p-0">
        <div class="homepage-hero-module">
            <div class="video-container">
                <div class="filter"></div>
                <video autoplay loop class="fillWidth lazy" muted>
                    <source src="https://s3.ap-south-1.amazonaws.com/mukesh-jaiswal.in/Homepage_Hero_Video_Edit.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
<!--                    <source src="img/office.mp4" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.-->

                </video>
                <!--<div class="poster hidden">
                    <img src="img/cover_bg.jpg" alt="">
                </div>-->
                <div class="landing">
                    <div class="middeleText">
                        <div>
                            <h1 class="text-center">Hi I am Front End Developer, I</h1>
                            <h1 class="type-it"></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section2" class="gradient_work">
        <div class="container">
            <div class="title">
                <h1>Experiences</h1>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="border-box">
                        <h3>UI Design</h3>
                        <div class="circle" id="circles-1"></div>
                        <ul>
                            <li>Photoshop</li>
                            <li>Mockups</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="border-box">
                        <h3>UI Develop</h3>
                        <div class="circle" id="circles-2"></div>
                        <ul>
                            <li>HTML, CSS, JavaScript</li>
                            <li>Frameworks & Plugins</li>
                            <li>Dynamic Web pages</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="border-box">
                        <h3>Animations</h3>
                        <div class="circle" id="circles-3"></div>
                        <ul>
                            <li>CSS Animations</li>
                            <li>jQuery Animation</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="border-box">
                        <h3>Responsive UI</h3>
                        <div class="circle" id="circles-4"></div>
                        <ul>
                            <li>Mobile, Tablets & Large desktops</li>
                            <li>User interactive Grids, Columns</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section3" class="about_bg">
        <div class="container-fluid">
            <div class="title">
                <h1>Peojects</h1>
            </div>
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-lg-3 col-md-6 col-xs-12 p-0">
                    <div class="card project_1">
                        <div class="card-block">
                            <figure>
                                <img src="img/inhabitr.jpg" alt="inhabitr.com" title="Inhabitr.com" class="img-fluid">
                            </figure>
                            <div class="card-header">
                                <a href="#">Inhabitr - housing simplified</a>
                            </div>
                        </div>
                        <div class="card-footer">
                            <h4>Inhabitr - housing simplified</h4>
                            <a href="#" data-toggle="modal" data-target="#inhabitr">See details</a>
                        </div>
                        <div class="overlay_s"></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-xs-12 p-0">
                    <div class="card project_2">
                        <div class="card-block">
                            <figure>
                                <img src="img/bollywood_bubble.jpg" alt="Bollywoodbubble.com" title="Bollywoodbubble.com" class="img-fluid">
                            </figure>
                            <div class="card-header">
                                <a href="#">Bollywood Bubble</a>
                            </div>
                        </div>
                        <div class="card-footer">
                            <h4>Bollywood Bubble</h4>
                            <a href="#" data-toggle="modal" data-target="#bubble">See details</a>
                        </div>
                        <div class="overlay_s"></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-xs-12 p-0">
                    <div class="card project_3">
                        <div class="card-block">
                            <figure>
                                <img src="img/teamnest.jpg" alt="teamnest.com" title="teamnest.com" class="img-fluid">
                            </figure>
                            <div class="card-header">
                                <a href="#">Teamnest - HR Management</a>
                            </div>
                        </div>
                        <div class="card-footer">
                            <h4>Teamnest - HR Management</h4>
                            <a href="#" data-toggle="modal" data-target="#teamnest">See details</a>
                        </div>
                        <div class="overlay_s"></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-xs-12 p-0">
                    <div class="card project_4">
                        <div class="card-block">
                            <figure>
                                <img src="img/scrum7.jpg" alt="Scrum7" title="Scrum7" class="img-fluid">
                            </figure>
                            <div class="card-header">
                                <a href="#">Scrum7 - By Capgemini</a>
                            </div>
                        </div>
                        <div class="card-footer">
                            <h4>Scrum7 - By Capgemini</h4>
                            <a href="#" data-toggle="modal" data-target="#scrum7">See details</a>
                        </div>
                        <div class="overlay_s"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section4" class="white_bg">
        <div class="gradient_about"></div>
        <div class="container">
            <div class="row">
                <div class="title">
                    <h1>About</h1>
                </div>
                <div class="col-lg-7 col-sm-6">
                    <div class="middeleText">
                        <div class="about_me">
                            <h2>Here I am</h2>
                            <p>I am a Front End Developer in Mumbai working with a service based company, had completed more that 10 projects within a Year.</p>
                            <a href="#" data-toggle="modal" data-target="#profile" title="Mukesh Jaiswal">See more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-6">
                    <div class="profile_pic">
                        <figure>
                            <img src="img/Mukesh2.jpg" class="img-fluid" alt="Mukesh" title="Mukesh Jaiswal">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="fixed_bg"></div>
</div>


<!---- modals ----->
<div class="modal fade" id="teamnest" tabindex="-1" role="dialog" aria-labelledby="teamnest" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header gradient_header">
                <h5 class="modal-title"><img src="img/logo-footer.png" alt="Teamnest" title="Teamnest"> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Teamnest - HR Management</h3>
                <label>About Teamnest</label>
                <p>One platform. 8 services. Zero HR headaches. TeamNest is a cloud based service to automate core HR for small and medium sized organisations.</p>
                <label>Features</label>
                <ul>
                    <li>Attendance Management</li>
                    <li>Leaves Management</li>
                    <li>Expense Management</li>
                    <li>Helpdesk</li>
                    <li>Employee Self Services</li>
                </ul>
                <p>Available in Android/IOS App</p>
                <div class="modal-footer">
                    <a href="https://www.teamnest.com/" class="" target="_blank" title="Teamnest">Teamnest Website</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="profile" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header gradient_header">
                <h5 class="modal-title" id="teamnest">Mukesh Jaiswal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Online Renting places like Flats, Houses, Offices, etc in Chicago.</p>
                <label>About Mukesh</label>
                <p>some text</p>
                <div class="modal-footer">
                    <a href="#" target="_blank" title="Mukesh Jaiswal">Mukesh</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="inhabitr" tabindex="-1" role="dialog" aria-labelledby="inhabitr" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header gradient_header">
                <h5 class="modal-title" id="teamnest">Inhabitr - Housing simplified</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Online Renting places like Flats, Houses, Offices, etc in Chicago.</p>
                <label>About Inhabitr</label>
                <p>Looking for affordable and stylish furniture? Inhabitr Furniture Rental can assist you in getting all your needs satisfied! Rent Bedroom, Living room Packages or just rent any furniture item - Designer Sofa Sets, Elegant Bed Sets, Dining Sets or Home Office Furniture. Located in Chicago, we deliver, we assemble with no extra cost! Get quality furniture at amazingly low prices, with all the convenience you can ask for!</p>
                <div class="modal-footer">
                    <a href="https://www.inhabitr.com/" target="_blank" title="Inhabitr">Inhabitr Website</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bubble" tabindex="-1" role="dialog" aria-labelledby="bubble" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header gradient_header">
                <h5 class="modal-title" id="teamnest">Bollywood Bubble</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>About Bollywood Bubble</label>
                <p>Bollywood Bubble, a sister concern of Digi Osmosis LLP, is your one stop for all the hip and happening things going on in the tinsel town. From the hottest of gossips, to the latest news, events, and releases, we engage the subscribers with our crispy content, and the Bubble Bulletin makes sure that you don’t miss out on any important B-town news.</p>
                <label>Bollywood Features</label>
                <ul>
                    <li>News</li>
                    <li>Photos</li>
                    <li>Videos</li>
                    <li>Movies</li>
                    <li>Bubble TV</li>
                    <li>Lifestyle</li>
                </ul>
                <div class="modal-footer">
                    <a href="https://www.bollywoodbubble.com" class="" target="_blank" title="bollywoodbubble.com">bollywoodbubble Website</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="scrum7" tabindex="-1" role="dialog" aria-labelledby="scrum7" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header gradient_header">
                <h5 class="modal-title">Scrum7 By Capgemini</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>About Scrum7</label>
                <p>Scrum7 is a unique project at the confluence of sports, business, technology & innovation that demonstrates how technology enables people, whether in sports or business, to be ahead of the game.</p>
                <label>Scrum7 Features</label>
                <ul>
                    <li>Timeline</li>
                    <li>Themes</li>
                    <li>Rewards For Start-ups</li>
                </ul>
                <div class="modal-footer">
                    <a href="https://scrum7.dreamscape.co.in/" class="" target="_blank" title="Scrum7 By Capgemini">Scrum7 By Capgemini Website</a>
                </div>
            </div>
        </div>
    </div>
</div>


<?php @include 'footer.php' ?>
<script>
    $('body').scrollspy({ target: '#scrollSpy' });
    new TypeIt('.type-it', {
        strings: ["design & develop Web Pages", "make Responsive UIs", "design better user experiences"],
        speed: 50,
        breakLines: false,
        loop: true ,
        autoStart: false,
        nextStringDelay : 3000
    }).pause(2000).delete(105);

    var myCircle = Circles.create({
        id:                  'circles-1',
        radius:              50,
        value:               80,
        maxValue:            100,
        width:               4,
        text:                function(value){return value + '%';},
        colors:              ['#aab5d6', '#3f63d7'],
        duration:            400,
        wrpClass:            'circles-wrp',
        textClass:           'circles-text',
        valueStrokeClass:    'circles-valueStroke',
        maxValueStrokeClass: 'circles-maxValueStroke',
        styleWrapper:        true,
        styleText:           true
    });
    var myCircle = Circles.create({
        id:                  'circles-2',
        radius:              50,
        value:               75,
        maxValue:            100,
        width:               4,
        text:                function(value){return value + '%';},
        colors:              ['#aab5d6', '#3f63d7'],
        duration:            400,
        wrpClass:            'circles-wrp',
        textClass:           'circles-text',
        valueStrokeClass:    'circles-valueStroke',
        maxValueStrokeClass: 'circles-maxValueStroke',
        styleWrapper:        true,
        styleText:           true
    });
    var myCircle = Circles.create({
        id:                  'circles-3',
        radius:              50,
        value:               43,
        maxValue:            100,
        width:               4,
        text:                function(value){return value + '%';},
        colors:              ['#aab5d6', '#3f63d7'],
        duration:            400,
        wrpClass:            'circles-wrp',
        textClass:           'circles-text',
        valueStrokeClass:    'circles-valueStroke',
        maxValueStrokeClass: 'circles-maxValueStroke',
        styleWrapper:        true,
        styleText:           true
    });
    var myCircle = Circles.create({
        id:                  'circles-4',
        radius:              50,
        value:               90,
        maxValue:            100,
        width:               4,
        text:                function(value){return value + '%';},
        colors:              ['#aab5d6', '#3f63d7'],
        duration:            400,
        wrpClass:            'circles-wrp',
        textClass:           'circles-text',
        valueStrokeClass:    'circles-valueStroke',
        maxValueStrokeClass: 'circles-maxValueStroke',
        styleWrapper:        true,
        styleText:           true
    });
</script>
